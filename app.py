#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  Main application
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

import sys
import connection
from PySide import QtGui
from ui.ui_mainwindow import ui_mainWindow

if __name__ == "__main__":

    app = QtGui.QApplication(sys.argv)

    database = connection.database()

    #database.alterSetup()

    database.initDB()

    database.initTables()

    database.initTablesFields()

    mainWindow = ui_mainWindow(database)

    mainWindow.runUi()

    sys.exit(app.exec_())