Bismillahirrahmanirrahim

Alhamdulillah, atas berkat rahmat Allah SWT saya diberi kesempatan untuk
belajar tentang komputer dan pemrograman.
Salawat dan salam atas junjungan Nabi Muhammad saw. yang telah membawa cahaya
Islam kepada kita.

Tujuan:.
Ingin membuat program yang bisa digunakan di toko kami, Jaya Abadi a.k.a
samstore.
Yang memudahkan dan membantu rutinitas pengelolaan bisnis penjualan kami 
di sana.

Lingkup Kerja:
Toko ini dikelola dan dijalankan oleh pemiliknya sendiri. Ada 3 orang yang 
bekerja di dalamnya yang sekaligus merangkap sebagai penjaga.


A. Kepala/bos, yang bertugas:
   1. Mencatat barang yang habis
   2. Melakukan pemesanan barang; membuat catatan pemesanan dan menitipkannya
      kepada kurir(dalam hal ini supir-supir angkutan).
   3. Juga melakukan beberapa negosiasi harga/pemesanan via telepon.
   4. Mengkoordinasi pengiriman barang via kurir(supir angkutan).
   5. Membayar biaya pengiriman


B. Asisten kepala, yang bertugas:
   1. Mencatat barang lainnya yang habis(karena itemnya sangat banyak maka
      terjadi pembagian pengawasan dalam kategori-kategori tertentu)
   2. Menghitung jumlah pemasukan dan jumlah pengeluaran secara ingatan saja*.
   3. Menghitung utang-piutang yang pernah dilakukan terhadap pihak luar.
   4. Menaksir barang baru yang bisa diperjual-belikan.

C. Asisten gudang dan penjaga, bertugas:
   1. Mengelola penempatan barang di gudang/tempat penyimpanan
   2. Mencatat dan memastikan jumlah persedian barang (stok) di gudang
   3. Menjaga dan melayani pembeli di toko
   4. Menyiapkan catatan belanja barang ke kota
   

Info tambahan:
1. Jumlah item yang dikelola mencapai 1600-an(seribu enam ratus-an) buah item
2. Job deskripsi dari 3 orang tersebut sering bertukar
3. Pengelolaan keuangan, pencatatan barang masuk dan keluar hampir tidak di-
   dokumentasikan kecuali secara ingatan saja*
4. Tidak tersedianya meja khusus administrasi(kepala)
5. Pergerakan barang yang sangat cepat ditambah kurangnya personil pengelola


Tujuan aplikasi:
1. Mencatat jumlah pemasukan dan pengeluaran barang
2. Menandai persediaan barang yang menipis/habis[rusak] di gudang
3. Membuat draft daftar belanja barang
4. Mengkalkulasi jumlah pengeluaran dan pendapatan beserta biaya operasionalnya
5. Membuat laporan daftar belanja barang dalam kurun waktu 
   tertentu(bulan/minggu)
6. Mencatat harga beli, harga jual dan biaya operasional suatu barang, serta
   ikhtisar perubahannya seiring waktu
7. Lugas, mudah dan cepat dalam pengoperasiannya.




