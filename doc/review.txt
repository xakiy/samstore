dialog extension
dialog config
widgets windowflags
widgets slider/spinbox

widgets lineedit.py : input mask for phone, ISO date, license key; activating sub buttons
itemviews basicsortitemfilter.py : hot table filter based on regex, simple filter, etc
itemview spinboxdelegates.py : delegating value inside a table column
state machines twowaybutton.py : an on off button
richtext orderform.py: kind of report in tabs
graphicviews elasticnodes.py : elastics nodes, a soft movement of chained balls

