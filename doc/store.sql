-- MySQL dump 10.14  Distrib 5.5.29-MariaDB, for Linux (i686)
--
-- Host: localhost    Database: store
-- ------------------------------------------------------
-- Server version	5.5.29-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `db_kategori`
--

DROP TABLE IF EXISTS `db_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL DEFAULT '0',
  `kategori` varchar(60) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `tag` varchar(250) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kategori` (`kategori`),
  KEY `path` (`path`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_kategori_produk__db_produk_kategori`
--

DROP TABLE IF EXISTS `db_kategori_produk__db_produk_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_kategori_produk__db_produk_kategori` (
  `id` int(11) NOT NULL,
  `db_produk_id` int(11) NOT NULL,
  `db_kategori_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produk_id` (`db_produk_id`),
  KEY `db_kategori_id` (`db_kategori_id`),
  CONSTRAINT `db_kategori_produk__db_produk_kategori_db_kategori` FOREIGN KEY (`db_kategori_id`) REFERENCES `db_kategori` (`id`) ON DELETE CASCADE,
  CONSTRAINT `db_kategori_produk__db_produk_kategori_db_produk` FOREIGN KEY (`db_produk_id`) REFERENCES `db_produk` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_nota`
--

DROP TABLE IF EXISTS `db_nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_supplier_id` int(11) NOT NULL,
  `pembayaran` enum('KONTAN','BON') NOT NULL DEFAULT 'KONTAN',
  `harga_beli_total` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `tanggal` (`tanggal`),
  KEY `db_nota_db_supplier` (`db_supplier_id`),
  KEY `harga_beli_total` (`harga_beli_total`),
  KEY `pembayaran` (`pembayaran`),
  CONSTRAINT `db_nota_db_supplier` FOREIGN KEY (`db_supplier_id`) REFERENCES `db_supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_nota_produk__db_produk_nota`
--

DROP TABLE IF EXISTS `db_nota_produk__db_produk_nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_nota_produk__db_produk_nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_nota_id` int(11) NOT NULL,
  `db_produk_id` int(11) NOT NULL,
  `jumlah_beli` int(5) NOT NULL,
  `su` varchar(10) NOT NULL DEFAULT 'pcs',
  `harga_beli_satuan` decimal(11,2) NOT NULL,
  `harga_beli_grosir` decimal(11,2) NOT NULL,
  `harga_jual_satuan` decimal(11,2) NOT NULL,
  `harga_jual_grosir` decimal(11,2) NOT NULL,
  `harga_jual_grosir_diskon` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_nota_produk__db_produk_nota_db_nota` (`db_nota_id`),
  KEY `db_nota_produk__db_produk_nota_db_produk` (`db_produk_id`),
  KEY `harga_beli_grosir` (`harga_beli_grosir`),
  KEY `harga_beli_satuan` (`harga_beli_satuan`),
  KEY `harga_jual_satuan` (`harga_jual_satuan`),
  KEY `harga_jual_grosir` (`harga_jual_grosir`),
  CONSTRAINT `db_nota_produk__db_produk_nota_db_nota` FOREIGN KEY (`db_nota_id`) REFERENCES `db_nota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `db_nota_produk__db_produk_nota_db_produk` FOREIGN KEY (`db_produk_id`) REFERENCES `db_produk` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_produk`
--

DROP TABLE IF EXISTS `db_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(40) DEFAULT '0',
  `nama` varchar(160) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `tag` varchar(250) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_2` (`nama`),
  KEY `tag` (`tag`),
  KEY `nama` (`nama`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=1363 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_produk_atribut`
--

DROP TABLE IF EXISTS `db_produk_atribut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_produk_atribut` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_produk_id` int(11) NOT NULL,
  `atribut` varchar(200) NOT NULL,
  `nilai` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `produk_atribut` (`db_produk_id`,`atribut`),
  KEY `nilai` (`nilai`),
  KEY `atribut` (`atribut`),
  CONSTRAINT `db_produk_atribut_db_produk` FOREIGN KEY (`db_produk_id`) REFERENCES `db_produk` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_rak`
--

DROP TABLE IF EXISTS `db_rak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_rak` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `ukuran_tray` varchar(20) DEFAULT NULL,
  `jumlah_tray` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_rak_nota_produk__db_nota_produk_rak`
--

DROP TABLE IF EXISTS `db_rak_nota_produk__db_nota_produk_rak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_rak_nota_produk__db_nota_produk_rak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_rak_id` int(11) NOT NULL,
  `db_nota_produk_id` int(11) NOT NULL,
  `banyak` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `grade` enum('A','B','C','D','E') DEFAULT 'B',
  `kondisi` enum('A','B','C') DEFAULT 'A',
  `kadaluarsa` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `db_nota_produk_id` (`db_nota_produk_id`),
  KEY `tanggal` (`tanggal`),
  KEY `db_rak_id` (`db_rak_id`),
  KEY `grade` (`grade`),
  KEY `kondisi` (`kondisi`),
  KEY `kadaluarsa` (`kadaluarsa`),
  KEY `banyak` (`banyak`),
  CONSTRAINT `db_rak_nota_produk__db_nota_produk_rak_db_produk` FOREIGN KEY (`db_nota_produk_id`) REFERENCES `db_nota_produk__db_produk_nota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `db_rak_nota_produk__db_nota_produk_rak_db_rak` FOREIGN KEY (`db_rak_id`) REFERENCES `db_rak` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db_supplier`
--

DROP TABLE IF EXISTS `db_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_nama` varchar(200) NOT NULL,
  `db_industry_id` int(2) NOT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `kota` varchar(80) DEFAULT NULL,
  `ponsel` varchar(40) DEFAULT NULL,
  `telp` varchar(40) DEFAULT NULL,
  `fax` varchar(40) DEFAULT NULL,
  `situs` varchar(120) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `kontak_nama` varchar(200) NOT NULL,
  `kontak_telp` varchar(40) DEFAULT NULL,
  `kontak_ponsel` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `db_supplier_industry` (`db_industry_id`),
  CONSTRAINT `db_supplier_industry` FOREIGN KEY (`db_industry_id`) REFERENCES `list_industry` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `list_industry`
--

DROP TABLE IF EXISTS `list_industry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_industry` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama` varchar(60) NOT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `view_nota_items`
--

DROP TABLE IF EXISTS `view_nota_items`;
/*!50001 DROP VIEW IF EXISTS `view_nota_items`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_nota_items` (
  `id` tinyint NOT NULL,
  `db_nota_id` tinyint NOT NULL,
  `jumlah_beli` tinyint NOT NULL,
  `su` tinyint NOT NULL,
  `nama` tinyint NOT NULL,
  `harga_beli_satuan` tinyint NOT NULL,
  `harga_beli_grosir` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_produk_atribut`
--

DROP TABLE IF EXISTS `view_produk_atribut`;
/*!50001 DROP VIEW IF EXISTS `view_produk_atribut`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_produk_atribut` (
  `PK` tinyint NOT NULL,
  `ID` tinyint NOT NULL,
  `NAMA` tinyint NOT NULL,
  `ATRIBUT` tinyint NOT NULL,
  `NILAI` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_nota_items`
--

/*!50001 DROP TABLE IF EXISTS `view_nota_items`*/;
/*!50001 DROP VIEW IF EXISTS `view_nota_items`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=``@`` SQL SECURITY DEFINER */
/*!50001 VIEW `view_nota_items` AS select `np`.`id` AS `id`,`np`.`db_nota_id` AS `db_nota_id`,`np`.`jumlah_beli` AS `jumlah_beli`,`np`.`su` AS `su`,`p`.`nama` AS `nama`,`np`.`harga_beli_satuan` AS `harga_beli_satuan`,`np`.`harga_beli_grosir` AS `harga_beli_grosir` from (`db_nota_produk__db_produk_nota` `np` left join `db_produk` `p` on((`np`.`db_produk_id` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_produk_atribut`
--

/*!50001 DROP TABLE IF EXISTS `view_produk_atribut`*/;
/*!50001 DROP VIEW IF EXISTS `view_produk_atribut`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=``@`` SQL SECURITY DEFINER */
/*!50001 VIEW `view_produk_atribut` AS select `a`.`id` AS `PK`,`p`.`id` AS `ID`,`p`.`nama` AS `NAMA`,`a`.`atribut` AS `ATRIBUT`,`a`.`nilai` AS `NILAI` from (`db_produk` `p` left join `db_produk_atribut` `a` on((`p`.`id` = `a`.`db_produk_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-08 15:37:19
