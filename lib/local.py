# -*- coding: utf-8 -*-
"""
Lib. Locale
Language and Translation
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide.QtCore import QLocale

supportedLanguages = dict('Bahasa Indonesia':QLocale.Indonesian, 'English':QLocale.English)

countriesForLanguage = QLocale.countriesForLanguage

locale = QLocale(QLocale.Indonesian)
locale.setNumberOptions(QLocale.OmitGroupSeparator)
    
    
    

