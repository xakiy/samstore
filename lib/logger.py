# -*- coding: utf-8 -*-
"""
Lib. Simple Logger
Log something we need to trace
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide import QtGui, QtCore
import time
import config

class log(QtGui.QMessageBox):

	__commandLineDebug = config.application['commandLineDebug']
	__logDebug = config.application['logDebug']
	__debugTitle = ""
	__debugText = ""

	def __init__(self, parent = None):
		super(log, self).__init__(parent)


	def logMe(self, title = "", text = ""):		
		self.__debugTitle = title
		self.__debugText = text
		if self.__commandLineDebug:
			print '[ ' + time.ctime() + ' ] - ' + self.__debugTitle + ': ' + self.__debugText
		if self.__logDebug:
			pass # TODO


	def aboutApp(self):
		self.setWindowTitle("About")
		self.setText("<h3>Samstore</h3>")
		info = "<p>Aplikasi Penjualan dan Stok <b>%s</b><p/><p>Versi: %s</p>" % (config.application['store'], config.application['version'])
		self.setInformativeText(info)
		self.setStandardButtons(self.Ok)
		self.setVisible(True)


