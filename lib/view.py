# -*- coding: utf-8 -*-
"""
Lib. View
QTableView modified version
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide import QtGui
from lib import logger

class tableView(QtGui.QTableView):
    """
    Kelas Table View
    """

    def __init__(self, database, parent = None):

        super(tableView, self).__init__(parent)
        self.setObjectName('tableView')
        self.database = database
        self.log = logger.log()


    def hideColumns(self, hideColumns = None):
	"""
	Menyembunyikan kolom dalam parameter list yang diberikan
	"""
        self.hideColumns = hideColumns
              
        # Sembunyikan field tertentu
        if type(self.hideColumns) == list:
          if self.hideColumns.__len__() > 0:
            for column in self.hideColumns:
              self.log.logMe(title="Hiding", text="Column '%s' at index '%s'" % (column, self.model().record().indexOf(column)))
              self.setColumnHidden(self.model().record().indexOf(column), True)
