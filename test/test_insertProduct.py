#!/usr/bin/python
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'insertProduct.ui'
#
# Created: Mon Jul 23 22:50:12 2012
#      by: pyside-uic 0.2.13 running on PySide 1.1.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui, QtSql
import connection
        

class Ui_insertProduct(object):
    def __init__(self):
        self.db = connection.database()
        self.db.setup(db='store')
        self.db.initDB()
        self.db.initTables()
        self.db.initTablesFields()

        self.kategoriModel = QtSql.QSqlTableModel()
        self.kategoriModel.setQuery(QtSql.QSqlQuery("SELECT id, path, kategori FROM db_kategori ORDER BY kategori ASC"))
        self.kategoriCompleter = QtGui.QCompleter()
        self.kategoriCompleter.setModel(self.kategoriModel)
        self.kategoriCompleter.setCompletionColumn(2)
        
    def setupUi(self, insertProduct):
        insertProduct.setObjectName("insertProduct")
        insertProduct.resize(464, 480)

        self.formLayoutWidget = QtGui.QWidget(insertProduct)
        self.formLayoutWidget.setGeometry(QtCore.QRect(13, 19, 441, 401))
        self.formLayoutWidget.setObjectName("formLayoutWidget")

        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")

        self.itemCodeLabel = QtGui.QLabel(self.formLayoutWidget)
        self.itemCodeLabel.setObjectName("itemCodeLabel")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.itemCodeLabel)
        
        self.itemCodeEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.itemCodeEditor.setObjectName("itemCodeEditor")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.itemCodeEditor)
        
        self.namaLabel = QtGui.QLabel(self.formLayoutWidget)
        self.namaLabel.setObjectName("namaLabel")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.namaLabel)
        
        self.namaEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.namaEditor.setObjectName("namaEditor")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.namaEditor)
        
        self.slugLabel = QtGui.QLabel(self.formLayoutWidget)
        self.slugLabel.setObjectName("slugLabel")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.slugLabel)
        
        self.slugEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.slugEditor.setObjectName("slugEditor")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.slugEditor)

        self.tagLabel = QtGui.QLabel(self.formLayoutWidget)
        self.tagLabel.setObjectName("tagLabel")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.tagLabel)
        
        self.tagEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.tagEditor.setObjectName("tagEditor")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.tagEditor)

        self.kategoriLabel = QtGui.QLabel(self.formLayoutWidget)
        self.kategoriLabel.setObjectName("kategoriLabel")
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.kategoriLabel)
        
        self.kategoriComboBox = QtGui.QComboBox(self.formLayoutWidget)
        self.kategoriComboBox.setObjectName("kategoriComboBox")
        self.kategoriComboBox.setModel(self.kategoriModel)
        self.kategoriComboBox.setModelColumn(2)
        #self.kategoriComboBox.setMaxVisibleItems(10)
        self.kategoriComboBox.setEditable(True)
        self.kategoriComboBox.setCompleter(self.kategoriCompleter)
        self.kategoriCompleter.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        #self.kategoriCompleter.setCompletionMode(QtGui.QCompleter.UnfilteredPopupCompletion)
        self.kategoriCompleter.setCompletionMode(QtGui.QCompleter.InlineCompletion)
        self.kategoriCompleter.setModelSorting(QtGui.QCompleter.CaseInsensitivelySortedModel)
        self.kategoriCompleter.setWrapAround(False)
        #self.kategoriCompleter.setCompletionRole(3)
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.kategoriComboBox)
        
        self.keteranganLabel = QtGui.QLabel(self.formLayoutWidget)
        self.keteranganLabel.setObjectName("keteranganLabel")
        self.formLayout.setWidget(6, QtGui.QFormLayout.LabelRole, self.keteranganLabel)
        
        self.keteranganEditor = QtGui.QTextEdit(self.formLayoutWidget)
        self.keteranganEditor.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.keteranganEditor.setTabChangesFocus(True)
        self.keteranganEditor.setObjectName("keteranganEditor")
        self.formLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.keteranganEditor)
                
        self.buttonBox = QtGui.QDialogButtonBox(insertProduct)
        self.buttonBox.setObjectName("buttonBox")
        self.buttonBox.setGeometry(QtCore.QRect(40, 440, 201, 25))
        
        self.pushButtonSave = self.buttonBox.addButton(QtGui.QDialogButtonBox.Save)        
        self.discardButton = self.buttonBox.addButton(QtGui.QDialogButtonBox.Discard)
        self.resetButton = self.buttonBox.addButton(QtGui.QDialogButtonBox.Reset)
        
        self.itemCodeLabel.setBuddy(self.itemCodeEditor)
        self.namaLabel.setBuddy(self.namaEditor)
        self.slugLabel.setBuddy(self.slugEditor)
        self.tagLabel.setBuddy(self.tagEditor)
        self.kategoriLabel.setBuddy(self.kategoriComboBox)
        self.keteranganLabel.setBuddy(self.keteranganEditor)
        
        #print 'woo', self.variantEditor.text(), self.printSome(insertProduct)

        self.retranslateUi(insertProduct)
        #QtCore.QObject.connect(self.pushButtonSave, QtCore.SIGNAL("clicked()"), insertProduct.accept)
        #QtCore.QObject.connect(self.pushButtonSave, QtCore.SIGNAL("released()"), insertProduct.close)
        #QtCore.QMetaObject.connectSlotsByname(insertProduct)
        
        self.namaEditor.editingFinished.connect(self.makeTitle)
        self.namaEditor.editingFinished.connect(self.makeSlug)
        self.namaEditor.editingFinished.connect(self.makeTag)
        self.pushButtonSave.clicked.connect(self.insertData)
        self.resetButton.clicked.connect(self.resetAll)
        self.discardButton.clicked.connect(app.quit)



    def makeTag(self):
        tagString = self.namaEditor.text()
        if len(tagString) > 0:
           tagString = tagString.replace(' ', ', ').lower()
           self.tagEditor.setText(tagString)
        
        
    def makeSlug(self):
        slugString = self.namaEditor.text()
        if len(slugString) > 0:
           slugString = slugString.lower()
           self.slugEditor.setText(slugString)
    
    #@QtCore.Slot(self, str)
    def makeTitle(self):
        namaString = self.namaEditor.text()
        if len(namaString) > 0:
           namaString = namaString.title()
           self.namaEditor.setText(namaString)
           

    def retranslateUi(self, insertProduct):
        insertProduct.setWindowTitle(QtGui.QApplication.translate("insertProduct", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.itemCodeLabel.setText(QtGui.QApplication.translate("insertProduct", "&Kode Barang", None, QtGui.QApplication.UnicodeUTF8))
        self.namaLabel.setText(QtGui.QApplication.translate("insertProduct", "&Nama", None, QtGui.QApplication.UnicodeUTF8))
        self.slugLabel.setText(QtGui.QApplication.translate("insertProduct", "&Sebutan", None, QtGui.QApplication.UnicodeUTF8))
        self.tagLabel.setText(QtGui.QApplication.translate("insertProduct", "Ta&g", None, QtGui.QApplication.UnicodeUTF8))
        self.kategoriLabel.setText(QtGui.QApplication.translate("insertProduct", "Kateg&ori", None, QtGui.QApplication.UnicodeUTF8))
        self.keteranganLabel.setText(QtGui.QApplication.translate("insertProduct", "Ke&terangan", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonSave.setText(QtGui.QApplication.translate("insertProduct", "Sa&ve", None, QtGui.QApplication.UnicodeUTF8))
               
    
    def resetAll(self):
        # reset all fields to blank
        self.namaEditor.clear()
        self.slugEditor.clear()
        self.tagEditor.clear()
        self.itemCodeEditor.clear()
        self.keteranganEditor.clear()
        
        
    def validator(self):
        # stupid
        if self.namaEditor.text().__len__() > 0:
           return True

           
        
               
    def insertData(self):
        # we need data validator before inserting them into database
        # TODO: data validator
        
        # you need to add prepare method to database class instead of 
        # simple Query, so right now I bypass them, 
        # and use db.query directly.
        self.query = QtSql.QSqlQuery()
        # yes, we still use database 'samstore', check connection.py
        self.query.prepare("INSERT INTO db_produk(id, kode, nama, slug, tag, keterangan) \
                                  VALUES(:id, :kode, :nama, :slug, :tag, :keterangan)")
        self.query.bindValue(":id", '')
        self.query.bindValue(":kode", self.itemCodeEditor.text())
        self.query.bindValue(":nama", self.namaEditor.text())
        self.query.bindValue(":slug", self.slugEditor.text())
        self.query.bindValue(":tag", self.tagEditor.text())
        self.query.bindValue(":keterangan", 
        self.keteranganEditor.toPlainText())

        if self.query.exec_():
           print 'Insert OK'
           print self.query.boundValues()
           print self.query.lastInsertId()
           self.resetAll()
        else:
           print 'Insert Failed'
           print self.query.boundValues()



    

if __name__ == "__main__":
   import sys
   app = QtGui.QApplication(sys.argv)
   insProductDialog = QtGui.QDialog()
   
   UIinsertProduct = Ui_insertProduct()
   UIinsertProduct.setupUi(insProductDialog)
   insProductDialog.show()
   sys.exit(app.exec_())