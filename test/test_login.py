# -*- coding: utf-8 -*-

from PySide import QtGui, QtCore
import sys


class myForm(QtGui.QDialog):
      def __init__(self, parent=None):
          super(myForm, self).__init__(parent)
          self.setWindowTitle("my Login Form")
          self.setObjectName("myForm")
          self.resize(600, 400)
          
          self.layout = QtGui.QVBoxLayout()
          self.lineEdit = QtGui.QLineEdit()
          #self.button = QtGui.QPushButton("Push")
          
          self.buttonBox = QtGui.QDialogButtonBox(self)
          self.buttonBox.setGeometry(QtCore.QRect(80, 260, 291, 32))
          self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
          self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
          self.buttonBox.setObjectName("buttonBox")
          
          self.layout.addWidget(self.lineEdit)
          self.layout.addWidget(self.buttonBox)
          #self.layout.addWidget(self.button)
          
          self.setLayout(self.layout)
          
          # Working
          #QtCore.QObject.connect(self.button, QtCore.SIGNAL("clicked()"), self.accept)
          # Working too!
          QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
          
      
      
class popForm(QtGui.QDialog):
      def __init__(self, parent=None):
          super(popForm, self).__init__(parent)
          
          self.setWindowTitle("Horeee")
          
          self.layout = QtGui.QHBoxLayout()
          
          self.button = QtGui.QPushButton("Horeee ")
          self.label = QtGui.QLabel("Sorry, your click wont close the app, Ctrl+F4 instead")
          
          self.layout.addWidget(self.button)
          self.layout.addWidget(self.label)
          
          self.setLayout(self.layout)
          
          self.button.clicked.connect(self.warn)
          # SIGNAL finished akan dipanggil ketika form ini ditutup
          self.finished.connect(bye)
          
      def warn(self):
          global yourText
          print "I told you '%s', you can't do it" % yourText
          

def bye():
    print "bye '%s', see you!" % yourText


if __name__ == "__main__":
  
   app = QtGui.QApplication(sys.argv)
   
   MyForm = myForm()
   
   MyForm.show()
   
   if MyForm.exec_() == MyForm.Accepted:
      yourText = MyForm.lineEdit.text()
      print "You wrote \"%s\"" % yourText
      MyForm.hide()
      PopForm = popForm()
      PopForm.show()
      #sys.exit()
   
   # bye() bisa dipanggil lewat SIGNAL dari app atau 
   # dari dalam Dialog terakhir
   #app.lastWindowClosed.connect(bye)
   app.lastWindowClosed.connect(app.quit)
   app.exec_()
   
   
        
