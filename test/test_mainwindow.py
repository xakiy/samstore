# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Tue Sep 18 22:23:56 2012
#      by: pyside-uic 0.2.13 running on PySide 1.1.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
 
    def setupUi(self, MainWindow):
        
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 480)
        
        self.centralwidget = QtGui.QLabel('Hello Dear!')
        #self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        
        
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 640, 20))
        self.menubar.setObjectName("menubar")
        
        self.menuBerkas = QtGui.QMenu(self.menubar)
        self.menuBerkas.setObjectName("menuBerkas")
        
        self.menuSunting = QtGui.QMenu(self.menubar)
        self.menuSunting.setObjectName("menuSunting")
        
        self.menuTampil = QtGui.QMenu(self.menubar)
        self.menuTampil.setObjectName("menuTampil")
        
        self.menuBantuan = QtGui.QMenu(self.menubar)
        self.menuBantuan.setObjectName("menuBantuan")
        
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        
        self.actionBuka = QtGui.QAction(MainWindow)
        self.actionBuka.setObjectName("actionBuka")
        
        self.actionSimpan = QtGui.QAction(MainWindow)
        self.actionSimpan.setObjectName("actionSimpan")
        
        self.actionLaporan = QtGui.QAction(MainWindow)
        self.actionLaporan.setObjectName("actionLaporan")
        
        self.actionKategori = QtGui.QAction(MainWindow)
        self.actionKategori.setObjectName("actionKategori")
        
        self.actionDealer = QtGui.QAction(MainWindow)
        self.actionDealer.setObjectName("actionDealer")
        
        self.actionRak = QtGui.QAction(MainWindow)
        self.actionRak.setObjectName("actionRak")
        
        self.actionNota = QtGui.QAction(MainWindow)
        self.actionNota.setObjectName("actionNota")
        
        self.actionHarga = QtGui.QAction(MainWindow)
        self.actionHarga.setObjectName("actionHarga")
        
        self.actionTentang = QtGui.QAction(MainWindow)
        self.actionTentang.setObjectName("actionTentang")
        
        self.actionStatistik = QtGui.QAction(MainWindow)
        self.actionStatistik.setObjectName("actionStatistik")
        
        self.actionCari = QtGui.QAction(MainWindow)
        self.actionCari.setObjectName("actionCari")
        
        self.actionKeluar = QtGui.QAction(MainWindow)
        self.actionKeluar.setObjectName("actionKeluar")
        
        self.menuBerkas.addAction(self.actionBuka)
        self.menuBerkas.addAction(self.actionSimpan)
        self.menuBerkas.addSeparator()
        self.menuBerkas.addAction(self.actionLaporan)
        self.menuBerkas.addSeparator()
        self.menuBerkas.addAction(self.actionKeluar)
        
        self.menuSunting.addAction(self.actionStatistik)
        self.menuSunting.addSeparator()
        self.menuSunting.addAction(self.actionCari)
        
        self.menuTampil.addAction(self.actionDealer)
        self.menuTampil.addAction(self.actionRak)
        self.menuTampil.addSeparator()
        self.menuTampil.addAction(self.actionNota)
        self.menuTampil.addAction(self.actionHarga)
        
        self.menuBantuan.addAction(self.actionTentang)
        
        self.menubar.addAction(self.menuBerkas.menuAction())
        self.menubar.addAction(self.menuSunting.menuAction())
        self.menubar.addAction(self.menuTampil.menuAction())
        self.menubar.addAction(self.menuBantuan.menuAction())
        
        MainWindow.setCentralWidget(self.centralwidget)
        MainWindow.setMenuBar(self.menubar)
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.menuBerkas.setTitle(QtGui.QApplication.translate("MainWindow", "&Berkas", None, QtGui.QApplication.UnicodeUTF8))
        self.menuSunting.setTitle(QtGui.QApplication.translate("MainWindow", "&Sunting", None, QtGui.QApplication.UnicodeUTF8))
        self.menuTampil.setTitle(QtGui.QApplication.translate("MainWindow", "&Tampil", None, QtGui.QApplication.UnicodeUTF8))
        self.menuBantuan.setTitle(QtGui.QApplication.translate("MainWindow", "Bant&uan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionBuka.setText(QtGui.QApplication.translate("MainWindow", "&Buka", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSimpan.setText(QtGui.QApplication.translate("MainWindow", "&Simpan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionLaporan.setText(QtGui.QApplication.translate("MainWindow", "&Laporan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionKategori.setText(QtGui.QApplication.translate("MainWindow", "&Kategori", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDealer.setText(QtGui.QApplication.translate("MainWindow", "&Dealer", None, QtGui.QApplication.UnicodeUTF8))
        self.actionRak.setText(QtGui.QApplication.translate("MainWindow", "&Rak", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNota.setText(QtGui.QApplication.translate("MainWindow", "&Nota", None, QtGui.QApplication.UnicodeUTF8))
        self.actionHarga.setText(QtGui.QApplication.translate("MainWindow", "&Harga", None, QtGui.QApplication.UnicodeUTF8))
        self.actionTentang.setText(QtGui.QApplication.translate("MainWindow", "&Tentang", None, QtGui.QApplication.UnicodeUTF8))
        self.actionStatistik.setText(QtGui.QApplication.translate("MainWindow", "S&tatistik", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCari.setText(QtGui.QApplication.translate("MainWindow", "&Cari", None, QtGui.QApplication.UnicodeUTF8))
        self.actionKeluar.setText(QtGui.QApplication.translate("MainWindow", "&Keluar", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

