# -*- coding: utf-8 -*-

from PySide import QtCore, QtGui

import sys
import connection
from lib import model
from ui import ui_product


if __name__ == '__main__':
	
	app = QtGui.QApplication(sys.argv)
	
	database = connection.database()
	
	database.setup(db='store')
	
	database.initDB()
	
	database.initTables()
	
	database.initTablesFields()
	
	table = 'db_produk_atribut'
	
	headerList = database.getTableFields(table)
	
	tableModel = model.relationalTableModel(database)
	
	tableModel.setTable(table, headerList)
	
	tableModel.setRelation(1, 'db_produk', 'id', 'nama')
	
	tableModel.loadModel()
	
	dialogProduct = QtGui.QDialog()
	
	uiProduct = ui_product.Ui_Product()
	
	uiProduct.setupUi(dialogProduct, tableModel)
	
	# setting up modelTable into tableView
	
	uiProduct.tableView_Table.setModel(tableModel.model)
	
	uiProduct.tableView_Table.show()
	
	dialogProduct.show()
	
	app.lastWindowClosed.connect(database.closeDB)
	
	sys.exit(app.exec_())
	
	
	
	
	
	
