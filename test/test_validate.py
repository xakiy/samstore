# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'validate-nama.ui'
#
# Created: Wed Mar 13 14:15:49 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(441, 540)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(70, 490, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayoutWidget = QtGui.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(9, 9, 421, 481))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")

        self.namaLabel = QtGui.QLabel(self.gridLayoutWidget)
        self.namaLabel.setObjectName("namaLabel")
        self.gridLayout.addWidget(self.namaLabel, 0, 0, 1, 1)

        self.namaLineEdit = QtGui.QLineEdit(self.gridLayoutWidget)
        self.namaLineEdit.setObjectName("namaLineEdit")
        self.gridLayout.addWidget(self.namaLineEdit, 0, 1, 1, 2)

        self.namaValidator = nameValidator(self.namaLineEdit)
        self.namaLineEdit.setValidator(self.namaValidator)

        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 0, 1, 1)
        
        self.ponselLabel = QtGui.QLabel(self.gridLayoutWidget)
        self.ponselLabel.setObjectName("ponselLabel")
        self.gridLayout.addWidget(self.ponselLabel, 1, 0, 1, 1)
        
        self.ponselLineEdit = QtGui.QLineEdit(self.gridLayoutWidget)
        self.ponselLineEdit.setObjectName("ponselLineEdit")
        self.gridLayout.addWidget(self.ponselLineEdit, 1, 2, 1, 1)
        
        self.ponselCountryCodeComboBox = QtGui.QComboBox(self.gridLayoutWidget)
        self.ponselCountryCodeComboBox.setObjectName("ponselCountryCodeComboBox")
        self.gridLayout.addWidget(self.ponselCountryCodeComboBox, 1, 1, 1, 1)
        
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 2, 1, 1, 1)
        
        self.ponselLineEdit.setInputMask('+99 999 999 99999;_')
        self.ponselLineEdit.setCursorPosition(1)
        
        self.ponselCountryCodeComboBox.addItem('[ Negara ]', '')
        self.ponselCountryCodeComboBox.addItem('Indonesia', '62')
        self.ponselCountryCodeComboBox.addItem('Singapore', '63')
        
        self.ponselCountryCodeComboBox.activated[int].connect(self.setCountryCode)


        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.namaLabel.setText(QtGui.QApplication.translate("Dialog", "Nama", None, QtGui.QApplication.UnicodeUTF8))
        self.ponselLabel.setText(QtGui.QApplication.translate("Dialog", "Ponsel", None, QtGui.QApplication.UnicodeUTF8))

        
    def setCountryCode(self, index):
        if index == 0:
           return
        else:
           print self.ponselLineEdit.text()[3:]
           self.ponselLineEdit.setText(self.ponselCountryCodeComboBox.itemData(index) + self.ponselLineEdit.text()[3:])
           self.ponselLineEdit.setFocus()
           self.ponselLineEdit.setCursorPosition(4)



class nameValidator(QtGui.QValidator):

    def __init__(self, parent=None, minLength=2):
        super(nameValidator, self).__init__(parent)
        self.minLength = minLength
        self.maxLength = parent.maxLength()
        # Note: parent should be field that utilize this validator
        self.parent = parent
        self.whitespace = ("\n","\r","\r\n")
        print 'parent maxLength:', self.maxLength


    def validate(self, inputString, pos):
        print 'validating', inputString, pos
        length = len(inputString)
        if 0 < length <= self.maxLength:
            if self.whitespace.__contains__(inputString[pos-1]):
                print 'invalid'
            	return self.Invalid
            elif length < self.minLength:
                print 'intermediate'
                return self.Intermediate
            else:
                print 'acceptable'
                return self.Acceptable
    	else:
        	print 'invalid'
        	return self.Invalid


    def fixup(self, inputString):        
        self.parent.setText(inputString.strip())
        print 'aw, im called.'
        print 'it\'s a', inputString
        for i in self.whitespace:
            inputString = inputString.replace(i, ' ')
        self.parent.setText(inputString)
	
	
        



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

