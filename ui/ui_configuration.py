# -*- coding: utf-8 -*-
"""
UI Configuration
Provide widget for editing configurations
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

import sys, os, mine
sys.path = sys.path.__add__([os.path.abspath('.'),])
mine.listIt(sys.path)


from PySide import QtCore, QtGui
from ui.widgets import tabbedWidget
from lib import format
from lib.view import tableView
from lib.model import tableModel
import config
import qrc

class configurationWidget(tabbedWidget):

    def __init__(self, database, parent=None):

        super(configurationWidget, self).__init__(database, parent)
        
        self.setObjectName("configurationTabbedWidget")
        
        self.tabs.identitas = QtGui.QWidget()
        self.tabs.aplikasi = QtGui.QWidget()
        self.tabs.database = QtGui.QWidget()
        self.tabs.pengguna = QtGui.QWidget()

        # identitas start
        self.tabs.identitas.setObjectName("identitasTab")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/configuration"), QtGui.QIcon.Normal, QtGui.QIcon.Off)        
        self.addTab(self.tabs.identitas, icon, "I&dentitas")

        self.identitasTabVBoxLayout = QtGui.QVBoxLayout(self.tabs.identitas)

        self.dataTokoGroupBox = QtGui.QGroupBox(self.tabs.identitas)
        self.dataTokoGroupBox.setObjectName("groupBox")

        self.identitasTabGridLayout = QtGui.QGridLayout(self.dataTokoGroupBox)
        self.identitasTabGridLayout.setContentsMargins(11, 11, 11, 11)
        self.identitasTabGridLayout.setSpacing(9)
        self.identitasTabGridLayout.setObjectName("identitasTabGridLayout")
        
        self.namaTokoLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.namaTokoLabel.setObjectName("namaTokoLabel")
        self.identitasTabGridLayout.addWidget(self.namaTokoLabel, 0, 0, 1, 1)

        self.namaTokoLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.namaTokoLineEdit.setObjectName("namaTokoLineEdit")
        self.identitasTabGridLayout.addWidget(self.namaTokoLineEdit, 0, 1, 1, 1)
        
        self.pemilikLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.pemilikLabel.setObjectName("pemilikLabel")
        self.identitasTabGridLayout.addWidget(self.pemilikLabel, 1, 0, 1, 1)

        self.pemilikLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.pemilikLineEdit.setObjectName("pemilikLineEdit")
        self.identitasTabGridLayout.addWidget(self.pemilikLineEdit, 1, 1, 1, 1)

        self.alamatLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.alamatLabel.setObjectName("alamatLabel")
        self.identitasTabGridLayout.addWidget(self.alamatLabel, 2, 0, 1, 1)
        
        self.alamatPlainTextEdit = QtGui.QPlainTextEdit(self.tabs.identitas)
        self.alamatPlainTextEdit.setObjectName("alamatPlainTextEdit")
        self.identitasTabGridLayout.addWidget(self.alamatPlainTextEdit, 2, 1, 1, 1)
        
        self.teleponLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.teleponLineEdit.setObjectName("teleponLineEdit")
        self.identitasTabGridLayout.addWidget(self.teleponLineEdit, 3, 1, 1, 1)
        
        self.teleponLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.teleponLabel.setObjectName("label_4")
        self.identitasTabGridLayout.addWidget(self.teleponLabel, 3, 0, 1, 1)
        
        self.faxLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.faxLabel.setObjectName("faxLabel")
        self.identitasTabGridLayout.addWidget(self.faxLabel, 4, 0, 1, 1)

        self.faxLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.faxLineEdit.setObjectName("faxLineEdit")
        self.identitasTabGridLayout.addWidget(self.faxLineEdit, 4, 1, 1, 1)

        self.ponselLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.ponselLabel.setObjectName("ponselLabel")
        self.identitasTabGridLayout.addWidget(self.ponselLabel, 5, 0, 1, 1)

        self.ponselLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.ponselLineEdit.setObjectName("ponselLineEdit")
        self.identitasTabGridLayout.addWidget(self.ponselLineEdit, 5, 1, 1, 1)
        
        self.websiteLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.websiteLineEdit.setObjectName("websiteLineEdit")
        self.identitasTabGridLayout.addWidget(self.websiteLineEdit, 6, 1, 1, 1)
        
        self.websiteLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.websiteLabel.setObjectName("websiteLabel")
        self.identitasTabGridLayout.addWidget(self.websiteLabel, 6, 0, 1, 1)
        
        self.emailLabel = QtGui.QLabel(self.dataTokoGroupBox)
        self.emailLabel.setObjectName("emailLabel")
        self.identitasTabGridLayout.addWidget(self.emailLabel, 7, 0, 1, 1)
        
        self.emailLineEdit = QtGui.QLineEdit(self.dataTokoGroupBox)
        self.emailLineEdit.setObjectName("emailLineEdit")
        self.identitasTabGridLayout.addWidget(self.emailLineEdit, 7, 1, 1, 1)

        self.identitasButtonBox = QtGui.QDialogButtonBox(self)
        self.identitasButtonBox.setOrientation(QtCore.Qt.Horizontal)
        self.identitasButtonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.identitasButtonBox.setObjectName("identitasButtonBox")

        self.identitasTabVBoxLayout.addWidget(self.dataTokoGroupBox)
        self.identitasTabVBoxLayout.addWidget(self.identitasButtonBox)
        # identitas end

        # aplikasi start        
        self.tabs.aplikasi.setObjectName("aplikasiTab")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/system"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addTab(self.tabs.aplikasi, icon1, "&Aplikasi")

        self.aplikasiTabVBoxLayout = QtGui.QVBoxLayout(self.tabs.aplikasi)
        
        self.aplikasiLokalisasiGroupBox = QtGui.QGroupBox(self.tabs.aplikasi)
        self.aplikasiLokalisasiGroupBox.setObjectName("aplikasiLokalisasiGroupBox")

        self.aplikasiLokalisasiFormLayout = QtGui.QFormLayout(self.aplikasiLokalisasiGroupBox)
        self.aplikasiLokalisasiFormLayout.setObjectName("aplikasiLokalisasiFormLayout")
        self.aplikasiLokalisasiFormLayout.setContentsMargins(11, 11, 11, 11)
        self.aplikasiLokalisasiFormLayout.setSpacing(9)

        self.bahasaLabel = QtGui.QLabel(self.aplikasiLokalisasiGroupBox)
        self.bahasaLabel.setObjectName("bahasaLabel")
        self.aplikasiLokalisasiFormLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.bahasaLabel)
        self.bahasaComboBox = QtGui.QComboBox(self.aplikasiLokalisasiGroupBox)
        self.bahasaComboBox.setObjectName("bahasaComboBox")
        self.aplikasiLokalisasiFormLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.bahasaComboBox)
        
        self.negaraLabel = QtGui.QLabel(self.aplikasiLokalisasiGroupBox)
        self.negaraLabel.setObjectName("negaraLabel")
        self.aplikasiLokalisasiFormLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.negaraLabel)
        self.negaraComboBox = QtGui.QComboBox(self.aplikasiLokalisasiGroupBox)
        self.negaraComboBox.setObjectName("negaraComboBox")
        self.aplikasiLokalisasiFormLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.negaraComboBox)
        
        self.zonaWaktuLabel = QtGui.QLabel(self.aplikasiLokalisasiGroupBox)        
        self.zonaWaktuLabel.setObjectName("zonaWaktuLabel")
        self.aplikasiLokalisasiFormLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.zonaWaktuLabel)
        self.zonaWaktuComboBox = QtGui.QComboBox(self.aplikasiLokalisasiGroupBox)
        self.zonaWaktuComboBox.setObjectName("zonaWaktuComboBox")
        self.aplikasiLokalisasiFormLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.zonaWaktuComboBox)
        
        self.aplikasiTampilanGroupBox = QtGui.QGroupBox(self.tabs.aplikasi)
        self.aplikasiTampilanGroupBox.setObjectName("aplikasiTampilanGroupBox")

        self.aplikasiTampilanFormLayout = QtGui.QFormLayout(self.aplikasiTampilanGroupBox)
        self.aplikasiTampilanFormLayout.setObjectName("aplikasiTampilanFormLayout")
        self.aplikasiTampilanFormLayout.setContentsMargins(11, 11, 11, 11)
        self.aplikasiTampilanFormLayout.setSpacing(9)
        
        self.batasBarisLabel = QtGui.QLabel(self.aplikasiTampilanGroupBox)
        self.batasBarisLabel.setObjectName("batasBarisLabel")
        self.aplikasiTampilanFormLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.batasBarisLabel)
        self.batasBarisSpinBox = QtGui.QSpinBox(self.aplikasiTampilanGroupBox)
        self.batasBarisSpinBox.setObjectName("batasBarisSpinBox")
        self.aplikasiTampilanFormLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.batasBarisSpinBox)
        self.sembunyikanBarisLabel = QtGui.QLabel(self.aplikasiTampilanGroupBox)
        self.sembunyikanBarisLabel.setObjectName("sembunyikanBarisLabel")
        self.aplikasiTampilanFormLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.sembunyikanBarisLabel)
        self.sembunyikanBarisYaradioButton = QtGui.QRadioButton(self.aplikasiTampilanGroupBox)
        self.sembunyikanBarisYaradioButton.setObjectName("sembunyikanBarisYaradioButton")
        self.aplikasiTampilanFormLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.sembunyikanBarisYaradioButton)
        self.sembunyikanBarisTidakradioButton = QtGui.QRadioButton(self.aplikasiTampilanGroupBox)
        self.sembunyikanBarisTidakradioButton.setObjectName("sembunyikanBarisTidakradioButton")
        self.aplikasiTampilanFormLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.sembunyikanBarisTidakradioButton)

        self.aplikasiButtonBox = QtGui.QDialogButtonBox(self)
        self.aplikasiButtonBox.setOrientation(QtCore.Qt.Horizontal)
        self.aplikasiButtonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.aplikasiButtonBox.setObjectName("aplikasiButtonBox")

        self.aplikasiTabVBoxLayout.addWidget(self.aplikasiLokalisasiGroupBox)
        self.aplikasiTabVBoxLayout.addWidget(self.aplikasiTampilanGroupBox)
        self.aplikasiTabVBoxLayout.addWidget(self.aplikasiButtonBox)
        
        # aplikasi end

        # database start
        self.tabs.database.setObjectName("databaseTab")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/database"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addTab(self.tabs.database, icon2, "Da&tabase")


        # database end

        # pengguna start
        self.tabs.pengguna.setObjectName("penggunaTab")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/users"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addTab(self.tabs.pengguna, icon3, "&Pengguna")

        # pengguna end

        self.retranslateUi()
        self.setCurrentIndex(1)
        #QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
        #QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
        #QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        self.setWindowTitle(QtGui.QApplication.translate("configureDialog", "Configuration", None, QtGui.QApplication.UnicodeUTF8))

        self.dataTokoGroupBox.setTitle(QtGui.QApplication.translate("ToolBox", "Data Toko", None, QtGui.QApplication.UnicodeUTF8))
        self.namaTokoLabel.setText(QtGui.QApplication.translate("ToolBox", "Nama Toko ", None, QtGui.QApplication.UnicodeUTF8))
        self.alamatLabel.setText(QtGui.QApplication.translate("ToolBox", "Alamat", None, QtGui.QApplication.UnicodeUTF8))
        self.pemilikLabel.setText(QtGui.QApplication.translate("ToolBox", "Pemilik", None, QtGui.QApplication.UnicodeUTF8))
        self.ponselLabel.setText(QtGui.QApplication.translate("ToolBox", "Ponsel", None, QtGui.QApplication.UnicodeUTF8))
        self.faxLabel.setText(QtGui.QApplication.translate("ToolBox", "Fax", None, QtGui.QApplication.UnicodeUTF8))
        self.teleponLabel.setText(QtGui.QApplication.translate("ToolBox", "Telepon", None, QtGui.QApplication.UnicodeUTF8))
        self.websiteLabel.setText(QtGui.QApplication.translate("ToolBox", "Website", None, QtGui.QApplication.UnicodeUTF8))
        self.emailLabel.setText(QtGui.QApplication.translate("ToolBox", "Email", None, QtGui.QApplication.UnicodeUTF8))

        self.aplikasiLokalisasiGroupBox.setTitle(QtGui.QApplication.translate("ToolBox", "Lokalisasi", None, QtGui.QApplication.UnicodeUTF8))
        self.bahasaLabel.setText(QtGui.QApplication.translate("ToolBox", "Bahasa", None, QtGui.QApplication.UnicodeUTF8))
        self.negaraLabel.setText(QtGui.QApplication.translate("ToolBox", "Negara", None, QtGui.QApplication.UnicodeUTF8))
        self.zonaWaktuLabel.setText(QtGui.QApplication.translate("ToolBox", "Zona Waktu", None, QtGui.QApplication.UnicodeUTF8))
        
        self.aplikasiTampilanGroupBox.setTitle(QtGui.QApplication.translate("ToolBox", "Tampilan", None, QtGui.QApplication.UnicodeUTF8))
        self.batasBarisLabel.setText(QtGui.QApplication.translate("ToolBox", "Batas Baris", None, QtGui.QApplication.UnicodeUTF8))
        self.batasBarisLabel.setToolTip(QtGui.QApplication.translate("ToolBox", "<html><head/><body><p>Jumlah baris yang ditampilkan dalam setiap halaman query</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.batasBarisLabel.setWhatsThis(QtGui.QApplication.translate("ToolBox", "<html><head/><body><p>Jumlah baris yang ditampilkan dalam setiap halaman query</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.sembunyikanBarisLabel.setText(QtGui.QApplication.translate("ToolBox", "Sembunyikan Nomor Baris", None, QtGui.QApplication.UnicodeUTF8))
        self.sembunyikanBarisLabel.setToolTip(QtGui.QApplication.translate("ToolBox", "<html><head/><body><p>Bila Qt &gt;= 4.8 disarankan untuk disembunyikan, Qt 4.7 disarankan untuk ditampilkan.</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.sembunyikanBarisLabel.setWhatsThis(QtGui.QApplication.translate("ToolBox", "<html><head/><body><p>Ada bug di Qt 4.8 yang menyebabkan tampilan nomor baris menjadi buruk.</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.sembunyikanBarisYaradioButton.setText(QtGui.QApplication.translate("ToolBox", "Ya", None, QtGui.QApplication.UnicodeUTF8))
        self.sembunyikanBarisTidakradioButton.setText(QtGui.QApplication.translate("ToolBox", "Tidak", None, QtGui.QApplication.UnicodeUTF8))

        #self.setItemText(self.indexOf(self.tabs.identitas), QtGui.QApplication.translate("self", "Identitas", None, QtGui.QApplication.UnicodeUTF8))
        #self.setItemText(self.indexOf(self.tabs.aplikasi), QtGui.QApplication.translate("self", "Aplikasi", None, QtGui.QApplication.UnicodeUTF8))
        #self.setItemText(self.indexOf(self.tabs.database), QtGui.QApplication.translate("self", "Database", None, QtGui.QApplication.UnicodeUTF8))
        #self.setItemText(self.indexOf(self.tabs.pengguna), QtGui.QApplication.translate("self", "Pengguna", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)    
    configurationWdgt = configurationWidget(None)
    configurationWdgt.show()
    sys.exit(app.exec_())

