# -*- coding: utf-8 -*-
"""
UI Inserting Product
Form for inserting product
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide import QtCore, QtGui

class Ui_insertProduct(object):
    def setupUi(self, insertProduct):
        insertProduct.setObjectName("insertProduct")
        insertProduct.resize(464, 480)
        self.formLayoutWidget = QtGui.QWidget(insertProduct)
        self.formLayoutWidget.setGeometry(QtCore.QRect(13, 19, 441, 401))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.nameLabel = QtGui.QLabel(self.formLayoutWidget)
        self.nameLabel.setObjectName("nameLabel")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.nameLabel)
        self.nameEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.nameEditor.setObjectName("nameEditor")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.nameEditor)
        self.slugLabel = QtGui.QLabel(self.formLayoutWidget)
        self.slugLabel.setObjectName("slugLabel")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.slugLabel)
        self.slugEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.slugEditor.setObjectName("slugEditor")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.slugEditor)
        self.itemCodeLabel = QtGui.QLabel(self.formLayoutWidget)
        self.itemCodeLabel.setObjectName("itemCodeLabel")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.itemCodeLabel)
        self.itemCodeEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.itemCodeEditor.setObjectName("itemCodeEditor")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.itemCodeEditor)
        self.modelLabel = QtGui.QLabel(self.formLayoutWidget)
        self.modelLabel.setObjectName("modelLabel")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.modelLabel)
        self.modelEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.modelEditor.setObjectName("modelEditor")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.modelEditor)
        self.colorLabel = QtGui.QLabel(self.formLayoutWidget)
        self.colorLabel.setObjectName("colorLabel")
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.colorLabel)
        self.colorEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.colorEditor.setObjectName("colorEditor")
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.colorEditor)
        self.varianLabel = QtGui.QLabel(self.formLayoutWidget)
        self.varianLabel.setObjectName("varianLabel")
        self.formLayout.setWidget(5, QtGui.QFormLayout.LabelRole, self.varianLabel)
        self.variantEditor = QtGui.QLineEdit(self.formLayoutWidget)
        self.variantEditor.setObjectName("variantEditor")
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.variantEditor)
        self.descriptionLabel = QtGui.QLabel(self.formLayoutWidget)
        self.descriptionLabel.setObjectName("descriptionLabel")
        self.formLayout.setWidget(6, QtGui.QFormLayout.LabelRole, self.descriptionLabel)
        self.descriptionEditor = QtGui.QTextEdit(self.formLayoutWidget)
        self.descriptionEditor.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.descriptionEditor.setTabChangesFocus(True)
        self.descriptionEditor.setObjectName("descriptionEditor")
        self.formLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.descriptionEditor)
        self.pushButtonSave = QtGui.QPushButton(insertProduct)
        self.pushButtonSave.setGeometry(QtCore.QRect(350, 440, 95, 25))
        self.pushButtonSave.setCheckable(False)
        self.pushButtonSave.setObjectName("pushButtonSave")
        self.buttonBox = QtGui.QDialogButtonBox(insertProduct)
        self.buttonBox.setGeometry(QtCore.QRect(40, 440, 201, 25))
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Discard|QtGui.QDialogButtonBox.Reset)
        self.buttonBox.setObjectName("buttonBox")
        self.nameLabel.setBuddy(self.nameEditor)
        self.slugLabel.setBuddy(self.slugEditor)
        self.itemCodeLabel.setBuddy(self.itemCodeEditor)
        self.modelLabel.setBuddy(self.modelEditor)
        self.colorLabel.setBuddy(self.colorEditor)
        self.varianLabel.setBuddy(self.variantEditor)
        self.descriptionLabel.setBuddy(self.descriptionEditor)

        self.retranslateUi(insertProduct)
        QtCore.QObject.connect(self.pushButtonSave, QtCore.SIGNAL("clicked()"), insertProduct.accept)
        QtCore.QObject.connect(self.pushButtonSave, QtCore.SIGNAL("released()"), insertProduct.close)
        QtCore.QMetaObject.connectSlotsByName(insertProduct)

    def retranslateUi(self, insertProduct):
        insertProduct.setWindowTitle(QtGui.QApplication.translate("insertProduct", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.nameLabel.setText(QtGui.QApplication.translate("insertProduct", "&Nama", None, QtGui.QApplication.UnicodeUTF8))
        self.slugLabel.setText(QtGui.QApplication.translate("insertProduct", "&Sebutan", None, QtGui.QApplication.UnicodeUTF8))
        self.itemCodeLabel.setText(QtGui.QApplication.translate("insertProduct", "&Kode Barang", None, QtGui.QApplication.UnicodeUTF8))
        self.modelLabel.setText(QtGui.QApplication.translate("insertProduct", "&Model", None, QtGui.QApplication.UnicodeUTF8))
        self.colorLabel.setText(QtGui.QApplication.translate("insertProduct", "&Warna", None, QtGui.QApplication.UnicodeUTF8))
        self.varianLabel.setText(QtGui.QApplication.translate("insertProduct", "&Varian", None, QtGui.QApplication.UnicodeUTF8))
        self.descriptionLabel.setText(QtGui.QApplication.translate("insertProduct", "Ke&terangan", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonSave.setText(QtGui.QApplication.translate("insertProduct", "Save", None, QtGui.QApplication.UnicodeUTF8))

