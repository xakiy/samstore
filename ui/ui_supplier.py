# -*- coding: utf-8 -*-
"""
UI Supplier
Supplier related forms
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------


import sys, os, mine
sys.path = sys.path.__add__([os.path.abspath('.'),])
mine.listIt(sys.path)
import connection
import qrc
from PySide import QtCore, QtGui, QtSql
from lib.view import tableView
from lib.model import tableModel
from ui.supplier_form import supplierNewForm, supplierEditForm, supplierRemoveForm

class Ui_Dialog(object):

    def setupUi(self, Dialog):

        self.dialog = Dialog

        self.dialog.setObjectName("Dialog")
        self.dialog.resize(1024, 600)

        self.setupModel()
        self.setupDialogs()

        # Main Layout that holds frame
        mainVBoxLayout = QtGui.QVBoxLayout(self.dialog)
        self.dialog.setLayout(mainVBoxLayout)

        self.frame = QtGui.QFrame(self.dialog)
        #self.frame.setGeometry(QtCore.QRect(1, 1, 1024, 600))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName("frame")
        
        mainVBoxLayout.addWidget(self.frame)
        
        # Vertical Layout inside frame that has 2 rows
        inframeVLayout = QtGui.QVBoxLayout(self.frame)
        
        # Grid layout for supplierform and tableView
        inframeSubGridLayout = QtGui.QGridLayout()

        # table config
        self.tableName = 'view_supplier'
        self.tableHeaderList = self.database.getTableFields('list_industry')
        
        # model
        self.tableModel = tableModel(self.database)
        self.tableModel.setTable(self.tableName)
        self.tableModel.loadModel()

        # view
        self.tableView = tableView(self.database)
        self.tableView.setGeometry(QtCore.QRect(244, 24, 750, 460))
        self.tableView.setObjectName("tableView")
        self.tableView.setAlternatingRowColors(True)
        self.tableView.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tableView.setSortingEnabled(True)
        self.tableView.setModel(self.tableModel.model)
        self.tableView.hideColumns(['id', 'alamat', 'fax'])

        # FIX!
        self.addSupplierButton = QtGui.QToolButton()
        #self.addSupplierButton.setText("+")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/go_top"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        icon.addPixmap(QtGui.QPixmap(":/go_bottom"), QtGui.QIcon.Selected, QtGui.QIcon.On)
        self.addSupplierButton.setIcon(icon)
        self.addSupplierButton.setIconSize(QtCore.QSize(16, 20))
        self.addSupplierButton.setCheckable(True)
        self.addSupplierButton.setChecked(False)
        #self.addSupplierButton.setArrowType(QtCore.Qt.NoArrow)
        self.addSupplierButton.setObjectName("addSupplierButton")

        self.removeSupplierButton = QtGui.QToolButton()
        self.removeSupplierButton.setText("Remove")
        self.removeSupplierButton.setObjectName("removeSupplierButton")
        
        self.mainButtonBox = QtGui.QDialogButtonBox()
        self.mainButtonBox.setGeometry(QtCore.QRect(250, 490, 740, 25))
        self.mainButtonBox.setStandardButtons(QtGui.QDialogButtonBox.Apply|QtGui.QDialogButtonBox.Discard)
        self.mainButtonBox.setObjectName("mainButtonBox")
        
        inframeSubGridLayout.addWidget(self.tableView, 0, 2, 1, 1)

        inframeVLayout.addLayout(inframeSubGridLayout)

        # Horizontal layout that holds buttons
        inframeButtonHLayout = QtGui.QHBoxLayout()

        inframeButtonHLayout.addWidget(self.addSupplierButton)
        inframeButtonHLayout.addWidget(self.removeSupplierButton)
        inframeButtonHLayout.insertStretch(2, 1)
        inframeButtonHLayout.addWidget(self.mainButtonBox)

        inframeVLayout.addLayout(inframeButtonHLayout)

        self.retranslateUi(self.dialog)

        self.addSupplierButton.clicked.connect(self.newItemForm)
        self.removeSupplierButton.clicked.connect(self.removeItemForm)
        self.tableView.doubleClicked.connect(self.editItemForm)


    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))


    def setupModel(self):
        self.database = connection.database()
        self.database.initDB()
        self.database.initTables()
        self.database.initTablesFields()


    def setupDialogs(self):
        self.supplierNewForm = None
        self.supplierEditForm = None
        self.supplierRemoveForm = None


    def unCheck(self):
        if self.addSupplierButton.isChecked():
            self.addSupplierButton.setChecked(False)


    def reloadTable(self):
        self.unCheck()
        self.tableModel.reloadModel()
        

    def newItemForm(self):
        if isinstance(self.supplierNewForm, supplierNewForm) == False:
            self.supplierNewForm = supplierNewForm(self.database, 'New Supplier', self.dialog)
            self.supplierNewForm.rejected.connect(self.unCheck)
            self.supplierNewForm.formSaved.connect(self.reloadTable)
        self.supplierNewForm.exec_()


    def editItemForm(self, index):
        if isinstance(self.supplierEditForm, supplierEditForm) == False:
            self.supplierEditForm = supplierEditForm(self.database, 'Edit Supplier', self.dialog)
            self.supplierEditForm.formSaved.connect(self.tableModel.reloadModel)
        self.supplierEditForm.editSupplier(self.tableModel.model.data(self.tableModel.model.index(index.row(), 0)))
        self.supplierEditForm.exec_()
        
        
    def removeItemForm(self):
        if isinstance(self.supplierRemoveForm, supplierRemoveForm) == False:
            self.supplierRemoveForm = supplierRemoveForm(self.database, 'supplierRemoveForm')
            self.supplierRemoveForm.supplierRemoved.connect(self.tableModel.reloadModel)
        index = self.tableView.currentIndex()
        supplier_id = self.tableModel.model.data(self.tableModel.model.index(index.row(), 0))
        supplier_nama = self.tableModel.model.data(self.tableModel.model.index(index.row(), 1))        
        self.supplierRemoveForm.removeSupplier(supplier_id, supplier_nama)
        self.supplierRemoveForm.exec_()



if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

"""
TODO:
1. There should be only one form active either newForm or editForm.
2. After editForm succeed saving a record, the table should be refreshed.
"""