# -*- coding: utf-8 -*-
"""
UI Central Widget
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide import QtCore, QtGui
from lib.model import relationalModel
from ui.widgets import tabbedWidget

class centralWidget(tabbedWidget):

    def __init__(self, database, parent=None):

        super(centralWidget, self).__init__(database, parent)
        
        self.setObjectName("tabWidget")

        # Definisikan dulu tab-tab yang akan kita pakai di sini
        self.tabs.table = QtGui.QWidget()
        self.tabs.table.setObjectName("tabTable")
        self.addTab(self.tabs.table, "&Table")

        self.tabs.browser = QtGui.QWidget()
        self.tabs.browser.setObjectName("tabBrowser")
        self.addTab(self.tabs.browser, "Bro&wser")

        # berikut adalah self.tabs.table dan bagian-bagiannya
        
        self.tabs.table.tableName = 'db_produk_atribut'
        self.tabs.table.tableHeaderList = self.database.getTableFields(self.tabs.table.tableName)
        
        # model
        self.tabs.table.model = relationalModel(self.database)
        self.tabs.table.model.setTable(self.tabs.table.tableName)
        self.tabs.table.model.setRelation(1, 'db_produk', 'id', 'nama')
        self.tabs.table.model.loadModel()

        # view
        # FIX:
        # Use lib.view.viewTable instead of QTableView that provides column hiding
        self.tabs.table.view = QtGui.QTableView(self.tabs.table)
        self.tabs.table.view.setObjectName("tableView")
        self.tabs.table.view.setAlternatingRowColors(True)
        self.tabs.table.view.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tabs.table.view.setSortingEnabled(True)
        self.tabs.table.view.setModel(self.tabs.table.model.model)

        self.tabs.table.buttonBox = QtGui.QDialogButtonBox(self)
        self.tabs.table.buttonBox.setObjectName("buttonBox")
        self.tabs.table.buttonBox.setGeometry(QtCore.QRect(390, 360, 201, 41))
        self.tabs.table.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.tabs.table.buttonCancel = self.tabs.table.buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
        self.tabs.table.buttonOk = self.tabs.table.buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
        self.tabs.table.buttonRefresh = self.tabs.table.buttonBox.addButton("Refresh", QtGui.QDialogButtonBox.ResetRole)

        self.tabs.table.verticalLayout = QtGui.QVBoxLayout(self.tabs.table)
        self.tabs.table.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.tabs.table.verticalLayout.setObjectName("verticalLayout")
        self.tabs.table.verticalLayout.addWidget(self.tabs.table.view)
        self.tabs.table.verticalLayout.addWidget(self.tabs.table.buttonBox)

        self.tabs.table.buttonRefresh.clicked.connect(self.tabs.table.model.reloadModel)

        # tab yang aktif sejak awal
        self.setCurrentIndex(0)



