# -*- coding: utf-8 -*-
"""
UI Custom Widgets
Provide some customized widgets
"""

#---------------------------------------------------------------------------
# Copyright (c) 2014 Ahmad Ghulam Zakiy <ghulam.zakiy@gmail.com>
# License: GPL3
#
# This file is part of SimpleStore
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#---------------------------------------------------------------------------

from PySide import QtGui, QtCore
from lib.group import group

class tabbedWidget(QtGui.QTabWidget):

    def __init__(self, database, parent=None):

        super(tabbedWidget, self).__init__(parent)
        
        #self.setTabPosition(QtGui.QTabWidget.West)
        #self.setTabShape(QtGui.QTabWidget.Triangular)
        self.setDocumentMode(False)

        self.database = database
        self.tabs = group()



class searchLineEdit(QtGui.QLineEdit):

    done = QtCore.Signal()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Return:
            self.done.emit()
        else:
            super(searchLineEdit, self).keyPressEvent(event)
